// npm init
// npm install express
// npm install mongoose@6.10.0
// touch index.js
// .gitignore
// 		node_modules


const express= require("express");
const mongoose= require("mongoose")

const app= express();
const port= 4004;

// Allows us to read json data
app.use(express.json())

// Allows us to read data from forms
app.use(express.urlencoded({extended: true}));

// [SECTION] MongoDB Connection
// Connect to the database by passing in your connection string, remember to replace the password.

// Connecting to MongoDB Atlas
// Syntax mongoose.connect("MongoDB connection string", useNewUrlParser: true)

mongoose.connect("mongodb+srv://tejuco-voren:Cheersbr021@zuitt-bootcamp.muegp8u.mongodb.net/s35-discussion?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db= mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", ()=> console.log("We're connected to the cloud database"))

// [SECTION] Mongoose Schemas
	// Schemas determine the structure of the documets to be written in the database

const taskSchema= new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// [SECTION] Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema

// MongoDB Model should always be Capital & Singular
const Task= mongoose.model("Task", taskSchema);

// [SECTION] Routes

// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		-If the task already exists in  the database, we return an error
		-If the task doesnt exist in the database, we add the task in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status " property does not need to be provided because our schema defaults it to "pending" upon creation of an object.

*/

app.post("/tasks", (request, response)=> {

	// Checks if there are duplicate tasks
	Task.findOne({name: request.body.name}, (error, result)=> {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		}

		let newTask= new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask)=> {
			if(error){
				return console.error(error)
			} else{
				return response.status(200).send('New task created!')
			}
		})
	})
})

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print error
3. If no errors, send a success status back to client/postman and return an array of documents
*/
app.get("/tasks", (req, res)=> {

	// "find" is a Mongoose Method that is similar to MongoDB 'find', and empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result)=> {

		// If an error occured
		if(err){

			// print any errors found in the console
			return console.log(err);

			// if no errors are found, then
		} else {

			// return the result
			// the ".json" method allows to send a JSON format for the response
			// the returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data: result
			})
		}
	})
})

//  Listen to the port
app.listen(port, () => console.log(`Server is now running at port ${port}`))