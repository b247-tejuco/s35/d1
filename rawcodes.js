const express= require("express");
const mongoose= require("mongoose")

const app= express();
const port= 4004;


app.use(express.json())
app.use(express.urlencoded({extended: true}));


mongoose.connect("mongodb+srv://tejuco-voren:Cheersbr021@zuitt-bootcamp.muegp8u.mongodb.net/?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db= mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", ()=> console.log("We're connected to the cloud database"))



const taskSchema= new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})




const Task= mongoose.model("Task", taskSchema);



app.post("/tasks", (request, response)=> {

	Task.findOne({name: request.body.name}, (error, result)=> {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		}

		let newTask= new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask)=> {
			if(error){
				return console.error(error)
			} else{
				return response.status(200).send('New task created!')
			}
		})
	})
})


app.get("/tasks", (req, res)=> {
	Task.find({}, (err, result)=> {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is now running at port ${port}`))